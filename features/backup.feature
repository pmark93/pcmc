Feature: User can create and download backups

    ## PCM = Personal credential manager
    ## cPCM = cloud personal credential manager
    ## VC = verifiable credential
    ## VP = verifiable presentation


  Background: User is logged in and on the wallet backup screen
    Given Web UI cPCM is up
    And User is logged in
    And User is on cPCM Wallet backup screen

    # Scenario: User successfully restores backup
    #    When user backups from local file
    #    Then user can see all VPs, VCs and VP history log contained within file

    # Scenario: User gets error message on invalid backup restore
    #     When user attempts to backup from invalid local file
    #     Then an error message occurs

    Scenario: User can perform backup sequence
      When user create a backup in the UI with name 'MarkBackup'
      Then scans the 'upload' link from screen
        And upload a file
      Then click the 'download' with the matching name
        And download the file
      Then compare files
        And files are matching

  # 1 create a backup in the UI - click add button
  # 2 scan the upload link from the screen (QR)
  # 3 take the link and upload any random file (post API)
  # 4 click the download
  # 5 scan the download link again (QR)
  # 6 call the link
  # 7 compare the files (uploaded - downloaded should be equal)
  # 8 create a download link again
  # 9 delete backup in the UI
  # 10 try download again - should fail


#  Scenario: User can export backup file
#    When user attempts to create a backup locally
#    Then user is prompted on where to save local backup file
#    When user selects save location
#    Then backup file is generated at that location
#
#  Scenario: User can create export backup file
#    When user creates a new backup
#    Then a new backup snapshot is available for download
