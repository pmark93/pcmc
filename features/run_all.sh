#!/usr/bin/env bash

# Define the number of times to repeat and the number of parallel jobs
repeat_count=50
parallel_jobs=5

# Function to run the feature file
run_feature() {
  echo "Running parallel_user_testing.feature iteration $1"
  behave parallel_user_testing.feature
}

export -f run_feature

# Use seq to generate a sequence of numbers from 1 to $repeat_count, and pipe it to parallel
seq $repeat_count | parallel -j $parallel_jobs run_feature

echo "All tests completed."

